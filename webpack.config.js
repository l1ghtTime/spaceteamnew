const path = require('path')                                //Встроеный модуль в node.js (path).Позволяет работать с путями на платформе.
const HTMLWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development'
const isProd = !isDev

const optimization = () => {
    const config =  {
        splitChunks: {
            chunks: 'all'
        }
    }

    if (isProd) {
        config.minimizer = [
            new OptimizeCssAssetsWebpackPlugin(),
            new TerserWebpackPlugin()
        ]
    }

    return config
}

const filename = ext => isDev ? `[name].${ext}` : `[name].[hash].${ext}`

const cssLoaders = extra => {
    const loaders = [
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
              hmr: isDev,
              reloadAll: true
          },
        }, 
        'css-loader'
    ]

    if(extra) {
        loaders.push(extra)
    }

    return loaders
}


module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    entry: '@/main.js',
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'dist')  // Вернет строчку с путем(коректная запись)
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },
    optimization: optimization(),
    plugins: [
        new HTMLWebpackPlugin({
            template: './index.html',
            minify: {
                collapseWhitespace: isProd
            }
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: filename('css')
        }),
        new CopyPlugin([
            { 
                from: './img',
                to: 'img' 
            }
        ]),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: cssLoaders()
            }, 
            {
                test: /\.s[ac]ss$/,
             
                use: cssLoaders('sass-loader')
            },
            {
                test: /\.bootstrap$/,
                use: cssLoaders('sass-loader')
            },
            {
                test: /\.(png|jpg|svg)$/,
                loader: 'file-loader',
                options: { 
                    name: '[path][name].[ext]',
                    emitFile: false 
                }
            }
        ]
    }
}