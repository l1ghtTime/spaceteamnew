/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./main.js","vendors~main"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./css/main.css":
/*!**********************!*\
  !*** ./css/main.css ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n    if(false) { var cssReload; }\n  \n\n//# sourceURL=webpack:///./css/main.css?");

/***/ }),

/***/ "./css/main.sass":
/*!***********************!*\
  !*** ./css/main.sass ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n    if(false) { var cssReload; }\n  \n\n//# sourceURL=webpack:///./css/main.sass?");

/***/ }),

/***/ "./img/spaceman.svg":
/*!**************************!*\
  !*** ./img/spaceman.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/spaceman.svg\");\n\n//# sourceURL=webpack:///./img/spaceman.svg?");

/***/ }),

/***/ "./main.js":
/*!*****************!*\
  !*** ./main.js ***!
  \*****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _css_main_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css/main.css */ \"./css/main.css\");\n/* harmony import */ var _css_main_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_main_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _css_main_sass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./css/main.sass */ \"./css/main.sass\");\n/* harmony import */ var _css_main_sass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_main_sass__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ \"../node_modules/bootstrap/dist/css/bootstrap.css\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _img_spaceman_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./img/spaceman.svg */ \"./img/spaceman.svg\");\n\r\n\r\n\r\n\r\n\r\nconst longitudeValue = document.querySelector('.longitude-value'),\r\n      latitudeValue = document.querySelector('.latitude-value'),\r\n      hour = document.getElementById('time'),\r\n      fullDate = document.getElementById('full-date'),\r\n      countHuman = document.getElementById('count'),\r\n      person = document.getElementById('person');\r\n\r\nconst monthNames = [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\",\r\n                    \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"\r\n];\r\n\r\nconst daysWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];\r\n\r\nlet coords, count, people;\r\n      \r\nconst currentLocation = async () => {\r\n    await fetch('http://api.open-notify.org/iss-now.json')\r\n        .then(response => response.json())\r\n        .then(resolve => coords = resolve.iss_position);\r\n        \r\n        longitudeValue.innerHTML = `${coords.longitude}`;\r\n        latitudeValue.innerHTML = `${coords.latitude}`;\r\n\r\n        initMap(latitudeValue.innerHTML , longitudeValue.innerHTML);\r\n};\r\n\r\nconst currentDate = () => {\r\n   const hours = new Date().toLocaleTimeString('en-GB', {\r\n    hour: 'numeric',\r\n    minute: 'numeric'\r\n  });\r\n\r\n  const date = new Date().getUTCDate(),\r\n        month = new Date().getUTCMonth(),\r\n        year = new Date().getUTCFullYear(),\r\n        week = new Date().getUTCDay(),\r\n        dateStr = `${date} ${monthNames[month]} ${year}`;\r\n        \r\n        hour.innerHTML = `${hours}`;\r\n        fullDate.innerHTML = `${daysWeek[week]}, ${dateStr}`;\r\n};\r\n\r\nconst initMap = (lat, lng) => {\r\n    let myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng)};\r\n\r\n    let map = new google.maps.Map(document.getElementById('map'), {\r\n      zoom: 4,\r\n      center: myLatLng\r\n    });\r\n\r\n    let marker = new google.maps.Marker({\r\n      position: myLatLng,\r\n      map: map,\r\n      title: 'Hello World!'\r\n    });\r\n};\r\n\r\nconst peopleData = async() => {\r\n  await fetch('http://api.open-notify.org/astros.json')\r\n      .then(response => response.json())\r\n      .then(resolve => {\r\n        count = resolve.number;\r\n        people = resolve.people;\r\n      });\r\n      \r\n      cleateElement(people);\r\n      innerHTML(count);\r\n};\r\n\r\nconst cleateElement = (people) => {\r\n  const itemArr = people.map((obj) => obj.name);\r\n  const item = itemArr.forEach(item => {\r\n    \r\n    const el = document.createElement('div');\r\n    el.id = 'person-box';\r\n    el.innerHTML = `<img src=\"img/spaceman.svg\" class=\"icon\" alt=\"icon\"><span class=\"name\">${item}</span>`\r\n      \r\n    person.appendChild(el);\r\n  });\r\n};\r\n\r\nconst innerHTML = count => {\r\n  countHuman.innerHTML = `${count}`;\r\n};\r\n\r\n\r\ncurrentLocation();\r\ncurrentDate();\r\npeopleData();\r\n\r\nsetInterval(() => {\r\n    currentLocation();\r\n    currentDate();\r\n}, 5000);\r\n\n\n//# sourceURL=webpack:///./main.js?");

/***/ })

/******/ });