import './css/main.css';
import './css/main.sass';
import 'bootstrap/dist/css/bootstrap.css';
import img from './img/spaceman.svg';

const longitudeValue = document.querySelector('.longitude-value'),
      latitudeValue = document.querySelector('.latitude-value'),
      hour = document.getElementById('time'),
      fullDate = document.getElementById('full-date'),
      countHuman = document.getElementById('count'),
      person = document.getElementById('person');

const monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
];

const daysWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

let coords, count, people;
      
const currentLocation = async () => {
    await fetch('http://api.open-notify.org/iss-now.json')
        .then(response => response.json())
        .then(resolve => coords = resolve.iss_position);
        
        longitudeValue.innerHTML = `${coords.longitude}`;
        latitudeValue.innerHTML = `${coords.latitude}`;

        initMap(latitudeValue.innerHTML , longitudeValue.innerHTML);
};

const currentDate = () => {
   const hours = new Date().toLocaleTimeString('en-GB', {
    hour: 'numeric',
    minute: 'numeric'
  });

  const date = new Date().getUTCDate(),
        month = new Date().getUTCMonth(),
        year = new Date().getUTCFullYear(),
        week = new Date().getUTCDay(),
        dateStr = `${date} ${monthNames[month]} ${year}`;
        
        hour.innerHTML = `${hours}`;
        fullDate.innerHTML = `${daysWeek[week]}, ${dateStr}`;
};

const initMap = (lat, lng) => {
    let myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng)};

    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: myLatLng
    });

    let marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });
};

const peopleData = async() => {
  await fetch('http://api.open-notify.org/astros.json')
      .then(response => response.json())
      .then(resolve => {
        count = resolve.number;
        people = resolve.people;
      });
      
      cleateElement(people);
      innerHTML(count);
};

const cleateElement = (people) => {
  const itemArr = people.map((obj) => obj.name);
  const item = itemArr.forEach(item => {
    
    const el = document.createElement('div');
    el.id = 'person-box';
    el.innerHTML = `<img src="img/spaceman.svg" class="icon" alt="icon"><span class="name">${item}</span>`
      
    person.appendChild(el);
  });
};

const innerHTML = count => {
  countHuman.innerHTML = `${count}`;
};


currentLocation();
currentDate();
peopleData();

setInterval(() => {
    currentLocation();
    currentDate();
}, 5000);
